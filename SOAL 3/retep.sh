#!/bin/bash

log_file="Register System/log.txt"

users_file="Register System/users.txt"

function log {
	echo "$(date +'%Y/%m/%d %H:%M:%S')  'REGISTER:' $1 $2" >> "$log_file"
}

function username_registered {
	if ! grep -q "$username" "$users_file"; then
	   echo -em"\nWrong username or password!"
	fi
}

function right_password {
	if password_hash =$(grep -w"^$username" "$users_file" | cut -d ':' -f 2); then
	if [["$password_hash" == "$password"]]; then
	   echo -e "\nLogin success!"
	log "INFO" "User $username logged in"
	else 
	   echo -e "\nLogin failed!"
	   log "ERROR" "Failed attempt on user $username"
	fi
}

function main {
	echo "Family Guy"
	echo "Welcome to Family Guy!"
	read -p "Please enter your username: " username
	read -sp "Please enter your password: " password 
	echo
}

main

if username_registered
then
   right_password
fi

