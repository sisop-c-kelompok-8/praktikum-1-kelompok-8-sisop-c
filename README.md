# PRAKTIKUM 1 KELOMPOK 8 SISOP C



# Praktikum Sisop Modul 1
Group Members:
1. Keysa Anadea (5025211028)
2. Anneu Tsabita (5025211026)
3. Dimas Pujangga (5025211212)

## Question 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
### 1A.
**_SOAL:_**
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. _Tampilkan 5 Universitas dengan ranking tertinggi di Jepang_

**_Solusi:_**
![Screenshot_2023-03-10_181515](/uploads/143d2b947e82c6b7af88ac209bc10a32/Screenshot_2023-03-10_181515.png)

**_Penjelasan Kode:_**
Kode diatas berfungsi untuk mencari kata "Japan" dalam file '2023UNIRANK.csv', kemudian mengurutkan baris data tersebut berdasarkan kolom pertama secara numerik, dan mengeluarkan outputnya menjadi lima baris teratas. Kemudian, kode akan mencetak kolom pertama dan kedua dari lima baris hasil pencarian tersebut.

Berikut adalah penjelasan syntax secara lebih detail:

- **grep "Japan" 2023UNIRANK.csv**: mencari kata "Japan" dalam file '2023UNIRANK.csv'. ``Grep`` merupakan command di Linux yang digunakan untuk mencari pola dari sebuah text file.
- **sort -t ',' -k1 -n**: mengurutkan kolom pertama pada file 2023UNIRANK.csv (dipisahkan dengan koma). ``Sort`` merupakan command di Linux untuk mengurutkan baris pada file tertentu. ``-k1`` merupakan command untuk sorting pada seluruh kolom 1 pada.csv file dan ``-n`` merupakan command untuk sorting secara numerikal yang secara default dengan ascending.
- **head -5**: membatasi hasilnya hanya menjadi lima baris teratas. ``Head`` merupakan command pada Linux yang berfungsi untuk menampilkan beberapa line teratas dari sebuah file.
- **awk -F ',' '{print $1, $2}**': mencetak kolom pertama dan kedua dari hasil pencarian yang sudah diurutkan. ``Awk`` merupakan sebuah command di Linux yang berfungsi untuk memanipulasi dan memproses text file. ``Print`` digunakan untuk menampilkan data yang diinginkan dimana ``$1, $2`` merupakan urutan kolom yang dininginkan yaitu data kolom ke-1 dan ke-2.

_**OUTPUT**_
![Screenshot_2023-03-10_212251](/uploads/a4da0bcb99faf0077674c7afcbb022d4/Screenshot_2023-03-10_212251.png)


### 1B.
**_SOAL :_**
Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang _paling rendah diantara 5 Universitas di Jepang_. 

**_Solusi:_**
![Screenshot_2023-03-10_192609](/uploads/880e9ae0488c6a660bc01badd107dc4f/Screenshot_2023-03-10_192609.png)
**_Penjelasan Kode:_**
Kode diatas berfungsi untuk mencari kata "Japan" dalam file '2023UNIRANK.csv', kemudian mengambil lima baris terakhir dari hasil pencarian tersebut. Kode akan mengurutkan baris-baris tersebut berdasarkan kolom kesembilan secara numerik dan memberikan output hasil cetak kolom pertama, kedua, dan kesembilan dari lima baris hasil pencarian tersebut.

Berikut adalah penjelasan syntax secara lebih detail:

- **grep "Japan" 2023UNIRANK.csv**: mencari kata "Japan" dalam file '2023UNIRANK.csv'. ``Grep`` merupakan command di Linux yang digunakan untuk mencari pola dari sebuah text file.
- **tail -5**: membatasi hasilnya hanya menjadi lima baris terbawah. ``Tail`` merupakan command pada Linux yang berfungsi untuk menampilkan beberapa line terbawah dari sebuah file.
- **sort -t ',' -k9 -n**: mengurutkan kolom kesembilan pada file 2023UNIRANK.csv (dipisahkan dengan koma). ``Sort`` merupakan command di Linux untuk mengurutkan baris pada file tertentu. ``-k9`` merupakan command untuk sorting pada seluruh kolom 9 pada.csv file dan ``-n`` merupakan command untuk sorting secara numerikal yang secara default dengan ascending.
- **awk -F ',' '{print $1, $2, $9}**': mencetak kolom pertama dan kedua dari hasil pencarian yang sudah diurutkan. ``Awk`` merupakan sebuah command di Linux yang berfungsi untuk memanipulasi dan memproses text file. ``Print`` digunakan untuk menampilkan data yang diinginkan dimana ``$1, $2, $9`` merupakan urutan kolom yang dininginkan yaitu data kolom ke-1, ke-2, dan ke-9.

_**OUTPUT**_
![Screenshot_2023-03-10_212353](/uploads/d5bf57258d3ae89a494880d3b4f3c341/Screenshot_2023-03-10_212353.png)

### 1C.
**_SOAL :_**
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, _cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi._

**_Solusi:_**
![Screenshot_2023-03-10_192810](/uploads/8b1e74372242fa3023b9327f97b0db90/Screenshot_2023-03-10_192810.png)

**_Penjelasan Kode:_**
Kode diatas berfungsi untuk mencari kata "Japan" dalam file '2023UNIRANK.csv', kemudian mengurutkan baris-baris tersebut berdasarkan kolom kedua puluh secara numerik, dan mencetak outputnya hanya menjadi sepuluh baris teratas. Selanjutnya, kode akan mencetak hasil kolom pertama, kedua, keempat, dan kedua puluh dari sepuluh baris hasil pencarian tersebut.

Berikut adalah penjelasan syntax secara lebih detail:

- **grep "Japan" 2023UNIRANK.csv**: mencari kata "Japan" dalam file '2023UNIRANK.csv'. ``Grep`` merupakan command di Linux yang digunakan untuk mencari pola dari sebuah text file.
- **sort -t ',' -k20 -n**: mengurutkan kolom pertama pada file 2023UNIRANK.csv (dipisahkan dengan koma). ``Sort`` merupakan command di Linux untuk mengurutkan baris pada file tertentu. ``-k20`` merupakan command untuk sorting pada seluruh kolom 20 pada.csv file dan ``-n`` merupakan command untuk sorting secra numerikal yang secara default dengan ascending.
- **head -10**: membatasi hasilnya hanya menjadi lima baris teratas. ``Head`` merupakan command pada Linux yang berfungsi untuk menampilkan beberapa line dari sebuah file.
- **awk -F ',' '{print $1, $2, $4, $20}**': mencetak kolom pertama dan kedua dari hasil pencarian yang sudah diurutkan. ``Awk`` merupakan sebuah command di Linux yang berfungsi untuk memanipulasi dan memproses text file. ``Print`` digunakan untuk menampilkan data yang diinginkan dimana ``$1, $2, $4, $20`` merupakan urutan kolom yang dininginkan yaitu data kolom ke-1, ke-2, ke-4, dan ke-20.

_**OUTPUT**_
![Screenshot_2023-03-10_212455](/uploads/d01d8651e5f6326964de482f9893e0a1/Screenshot_2023-03-10_212455.png)

### 1D.
**_SOAL :_**
Bocchi ngefans berat dengan universitas paling keren di dunia. _Bantu bocchi mencari universitas tersebut dengan kata kunci keren._

**_Solusi:_**
![Screenshot_2023-03-10_195344](/uploads/40ee07b175696a6325fe1e0673befba0/Screenshot_2023-03-10_195344.png)

**_Penjelasan Kode:_**
Kode diatas berfungsi untuk mencari kata "Keren" dalam file '2023UNIRANK.csv', kemudian mencetak outputnya.

Berikut adalah penjelasan syntax secara lebih detail:

- **awk -F ',' '/Keren/ {print $1, $2}'**': perintah untuk menjalankan program ``awk`` dengan opsi "-F" (field separator) yang disetel menjadi tanda koma (','). Setelah itu, program akan memproses sebuah file teks dan melakukan pencarian string "Keren" pada setiap barisnya. Jika string "Keren" ditemukan pada sebuah baris, maka program akan mencetak isi kolom pertama dan kedua dari baris tersebut menggunakan perintah ``print $1, $2``.

_**OUTPUT**_
![Screenshot_2023-03-10_212531](/uploads/18366eb27cf2349e6216c7ac88a2a513/Screenshot_2023-03-10_212531.png)

###Source Code:
![Screenshot_2023-03-10_195501](/uploads/072ab535fa2b5f20898972276b7eaf07/Screenshot_2023-03-10_195501.png)


<br>
<h2>Soal 2</h2>
<h4><strong>Kobeni Liburan</strong></h4>

Pada soal no. 2 ini akan dilakukan mendownload gambar tentang Indonesia sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Berikut preview dari shell script "kobeni_liburan.sh"  <br><br>

![source_code](/uploads/210468cb2270b3ad9efa362b1f1f21ff/source_code.jpg)

<h5>a. Menghitung jumlah gambar yang akan didownload</h5>
<ol type="i">
<li>Membuat variabel "hour" untuk menyimpan jam sekarang</li>

```
hour=$(date +"%H")
```

<li>Membuat variabel "countx" untuk menyimpan jumlah foto yang akan didownload sesuai jam sekarang. Jika pukul 00.00 akan mendownload satu gambar saja. Selain itu, akan didownload sesuai jam sekarang.</li>
</ol>

```
if [ "$hour" == 00 ]; then
    countx=1
else
    countx=$hour
fi
```

<h5>b. Menentukan nama folder dengan menghitung folder yang sudah ada</h5>
<ol type="i">
<li>Menghitung folder yang sudah ada untuk menentukan nama folder selanjutnya. Jika folder baru pertama kali dibuat, folder akan dinamakan "kumpulan_1", dan jika tidak akan dilanjutkan dari nama folder terakhir</li>

```
if [ "$folder_count" -eq "0" ]; then
    nama_folder="kumpulan_1"
else
    next_number=$((folder_count + 1))
    nama_folder="kumpulan_${next_number}"
fi
```

<li>Membuat folder dengan nama yang sudah ditentukan</li>
</ol>

```
mkdir -p "$nama_folder"
```

Code tersebut akan membuat folder baru bernama "kumpulan_*"

![folder](/uploads/35917880a2b34a563030bfe14c800230/folder.jpg)

Setelah itu, akan muncul message :

```
echo "file akan didownload sebanyak $countx kali"
```

![banyak_foto](/uploads/d5fdc81de339a15ae74c48184b00d3a6/banyak_foto.png)

<h5>c. Mendownload gambar dengan jumlah yang sudah ditentukan menggunakan while loop</h5>
<ol type="i">
<li>Membuat while loop dan variabel "index" untuk menyimpan urutan gambar yang sudah didownload</li>
</ol>

```
index=1
while [ "$index" -le "$countx" ]; do
    name_file="perjalanan_$index.jpg"
    index=$((index+1))
    wget -O "$nama_folder/$name_file" https://artikula.id/wp-content/uploads/2021/12/wayang.jpg
done
```

Code tersebut akan menghasilkan:

![perjalanan](/uploads/2262b01bcba9240c02cb7fde2a758a91/perjalanan.jpg)

<h5>d. Menjalankan crontab untuk mendownload gambar setiap 10 jam sekali</h5>
<ol type="i">
<li>Membuka file crontab</li>

Untuk membuka file crontab, pada terminal akan ditulis:

```
$ crontab -e
```

Lalu, menambahkan crontab untuk mendownload gambar setiap 10 jam:

![crontab](/uploads/18ee82f0dbaa40af8496782e8e203296/crontab.png)

<h5>e. Melakukan zip setiap satu hari</h5>
<ol type="i">
<li>Menentukan nama zip dengan menghitung zip yang sudah ada</li>

Pertama-tama, akan dihitung jumlah zip yang sudah ada dengan nama "devil_*"

```
jml_zip=$(ls -d devil_* 2>/dev/null | wc -l)
```

Lalu, akan dibuat variabel "zip_name" untuk menyimpan nama zip yang akan dibuat

```
zip_name="devil_$((jml_zip + 1))"
```

<li>Menentukan folder yang akan dizip</li>

Pertama-tama, akan dihitung jumlah folder yang ada untuk menentukan folder mana yang akan dizip. 

```
folder_count=$(ls -d kumpulan_* 2>/dev/null | wc -l)
let folder_count2=$folder_count-1
let folder_count3=$folder_count2-1
```

Jika jumlah folder merupakan kelipatan 5, folder yang akan dizip adalah sebanyak 2 folder. Jika bukan kelipatan 5, maka yang akan dizip adalah sebanyak 3 folder.

```
if [ $(expr $folder_count % 5) == 3 ]
then
    zip -r $zip_name kumpulan_$folder_count kumpulan_$folder_count2 kumpulan_$folder_count3
elif [ $(expr $folder_count % 5) == 0 ]
then
    zip -r $zip_name kumpulan_$folder_count kumpulan_$folder_count2
fi
```

Code tersebut akan menghasilkan:

![zip](/uploads/5ab4f4faceb0fa4c9af026424d408e60/zip.jpg)
<br>


## Question 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut:
1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username 
5. Tidak boleh menggunakan kata chicken atau ernie
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
#### Source Code:
louis.sh
![Screenshot_from_2023-03-10_20-30-47](/uploads/ee96e3f63e1786b25c1258d7bb200137/Screenshot_from_2023-03-10_20-30-47.png)
retep.sh
![Screenshot_from_2023-03-10_20-21-44](/uploads/d73fc2d1077862568fef062273925427/Screenshot_from_2023-03-10_20-21-44.png)

#### Explanation:
Membuat directory Register_Sytem untuk menyimpan log.txt & users.txt:
```
mkdir -p "Register_System"

log_file="Register_System/log.txt"

users_file="Register_System/users.txt"
```
Kemudian berikut isi dari users.txt dan log.txt:
users.txt
![Screenshot_from_2023-03-10_20-58-13](/uploads/797fb754132e176d5f3c2c541b7718bd/Screenshot_from_2023-03-10_20-58-13.png)
log.txt
![Screenshot_from_2023-03-10_20-57-40](/uploads/cd19d02370eb4b470c926b51daa6fe45/Screenshot_from_2023-03-10_20-57-40.png)

Format Register:
- Membuat fungsi log untuk mencetak message log di log.txt
```
function log {
	echo "$(date +'%Y/%m/%d %H:%M:%S')  'REGISTER:' $1 $2" >> "$log_file"
}
```

- Membuat fungsi untuk mengecek apakah username sudah terdaftar di users_file:
```
function check_username {
   if [ -e "$users_file" ]; then
       if [[ "$(grep "$username" "$users_file")" ]]; then
	   echo -e "\nUser already exist. Please try a different username!"
	   log "ERROR" "User already exist"
	   exit 1	
       fi
   fi
}
```
Apabila username sudah terdaftar, message: User already exist. Please try a different username!

- Membuat fungsi untuk mengecek apakah password sudah sesuai dengan ketentuan pada soal:
```
function check_password {
	if (( ${#password} < 8 )); then
	   echo "\nPassword must be at least 8 characters long!"
	   exit 1 
	elif [[ ! ${password} =~ [[:upper:]] || ! ${password} =~ [[:lower:]] ]]; then
	   echo -e "\nPassword must contain at least 1 uppercase letter 
	   and 1 lowercase letter!"
	   exit 1
	elif echo "$password" | grep -q -v ^[[:alnum:]]*$; then 
	   echo -e "\nPassword must contain alphanumeric character!"
	   exit 1
	elif [[ "$password" == "$username" ]]; then
	   echo -e "\nPassword cannot be the same as the username!"
	   exit 1
	elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
	   echo -e "\nPassword cannot use word 'chicken' or 'ernie'"
	   exit 1
	fi  
	   echo "$username:$password" >> "$users_file"
	   echo -e "\nAccount has been created!"
	   log "INFO" "User $username registered succesfully"  	   	
}
```
Format login
- Membuat fungsi log untuk mencetak message log di log.txt
```
function log {
	echo "$(date +'%Y/%m/%d %H:%M:%S')  'LOGIN:' $1 $2" >> "$log_file"
}
```

- Membuat fungsi username_registered untuk mengecek apakah username/password sudah terdaftar di users_file pada users.txt
```
function username_registered {
	if ! grep -q "$username" "$users_file" || ! grep -q "$password" "$users_file"; then
	   echo -e "\nWrong username or password!"
	   log "ERROR" "Failed login attempt on user $username"
	   exit 1
	fi
}
```
Apabila salah memasukkan username/password, message: Wrong username or password!

- Fungsi untuk mengecek username & password sesuai di users_file pada users.txt
```
function right_password {
	if grep -q "$username:$password" "$users_file"; then
	   echo -e "\nLogin success!"
	   log "INFO" "User $username logged in"
	fi
}
```
Message: Login success!



## Question #4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
- Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan


#### Source Code:
log_encrypt.sh:
![Screenshot_2023-03-10_202302](/uploads/6bfdce744629d031ae5d9d6df0c213b7/Screenshot_2023-03-10_202302.png)
log_decrypt.sh:
![Screenshot_2023-03-10_202906](/uploads/bcda786a8d36c681a66b9db4bbc7cdf0/Screenshot_2023-03-10_202906.png)

#### Explanation:
**log_encrypt.sh**

Penjelasan kode secara detail:
- ![Screenshot_2023-03-10_212124](/uploads/9f51ff3762e6c89343a4cbacc301ce13/Screenshot_2023-03-10_212124.png)
 Ini adalah dua variabel string yang berisi daftar huruf alfabet dalam huruf kecil dan huruf kapital. Deklarasi variable untuk menyimpan alfabet a-z dan A-Z sebanyak 2 kali. Hal ini dilakukan supaya pada saat melakukan dekripsi, setelah z bisa kembali lagi ke a. Daftar huruf ini digunakan sebagai plaintext dalam metode Caesar Cipher. 
- ![Screenshot_2023-03-10_212048](/uploads/a4fc1c56e94cd810a85deca476ed53fa/Screenshot_2023-03-10_212048.png)
 Ini adalah perintah untuk mendapatkan tanggal dan waktu saat ini, yang disimpan dalam variabel date. Format tanggal dan waktu yang digunakan adalah "jam:menit tanggal:bulan:tahun".
- ![Screenshot_2023-03-10_211940](/uploads/e4697c760b9687a6e556cc56eff766bb/Screenshot_2023-03-10_211940.png)
Ini adalah variabel string yang berisi nama file backup yang akan digunakan untuk menyimpan hasil enkripsi. Nama file backup ini mencakup tanggal dan waktu saat ini, dan akan disimpan di direktori "/home/keysanadea/SISOP/NO4/soal4encrypt/".
- ![Screenshot_2023-03-10_211832](/uploads/d6a6ea38ffc73a1c22c59358abe9e2c6/Screenshot_2023-03-10_211832.png): Ini adalah perintah untuk mendapatkan jam saat ini dalam format 24-jam, yang disimpan dalam variabel hour.
- ![Screenshot_2023-03-10_210837](/uploads/86cf9404172c36164bd61df3f943e7a1/Screenshot_2023-03-10_210837.png)
Ini adalah perintah utama untuk melakukan enkripsi. Pertama-tama, perintah cat /var/log/syslog digunakan untuk membaca isi file syslog. Kemudian, perintah tr digunakan untuk mengenkripsi isi file tersebut dengan metode Caesar Cipher. Setiap huruf pada plaintext akan diganti dengan huruf pada posisi yang sama di dalam daftar huruf yang digeser sebanyak nilai hour. Hasil enkripsi akan disimpan dalam file backup yang telah dibuat sebelumnya.
- **Encrypting dilakukan setiap 2 jam sekali menggunakan cronjob dengan syntax berikut:**
crontab -e 
Script cronjob :
0 */2 * * * /bin/bash /home/keysanadea/SISOP/NO4/log_encrypt.sh


**log_decrypt.sh**

Penjelasan kode secara detail:

- ![Screenshot_2023-03-10_211652](/uploads/fba41dd1bb38c4a89afa47b09124cfda/Screenshot_2023-03-10_211652.png)
 Ini adalah dua variabel string yang berisi daftar huruf alfabet dalam huruf kecil dan huruf kapital. Daftar huruf ini digunakan sebagai plaintext dalam metode Caesar Cipher.

- ![Screenshot_2023-03-10_211502](/uploads/efb761899cfccc04baaf83215021fa26/Screenshot_2023-03-10_211502.png)
 Ini adalah perintah untuk mencari file terbaru yang ada di direktori "/home/keysanadea/SISOP/NO4/soal4encrypt/" dan menyimpannya dalam variabel file_enc. File ini akan digunakan sebagai file yang akan didekripsi.

- ![Screenshot_2023-03-10_211135](/uploads/8d82b1557bec0dfa88bccccc6f20b5f7/Screenshot_2023-03-10_211135.png)
Ini adalah variabel string yang berisi path file yang akan dienkripsi dan path file hasil dekripsi, masing-masing.

- ![Screenshot_2023-03-10_211550](/uploads/bd8880b60255ee42cf784e81d207e66e/Screenshot_2023-03-10_211550.png)
Ini adalah perintah untuk mendapatkan jam saat ini dalam format 24-jam, yang disimpan dalam variabel current_hour.

- ![Screenshot_2023-03-10_211320](/uploads/985f1d0c79471096498cc913e91c9416/Screenshot_2023-03-10_211320.png)
Ini adalah perintah untuk melakukan pengecekan apakah file yang akan didekripsi ada di direktori "/home/keysanadea/SISOP/NO4/soal4encrypt/".

- ![Screenshot_2023-03-10_210943](/uploads/797b9836a3340cacfa5f27e32c854221/Screenshot_2023-03-10_210943.png)
 Ini adalah perintah utama untuk melakukan dekripsi. Pertama-tama, perintah cat digunakan untuk membaca isi file yang akan didekripsi. Kemudian, perintah tr digunakan untuk mendekripsi isi file tersebut dengan metode Caesar Cipher. Setiap huruf pada ciphertext akan diganti dengan huruf pada posisi yang sama di dalam daftar huruf plaintext yang digunakan dalam enkripsi. Hasil dekripsi akan disimpan dalam file dengan nama yang sama pada direktori "/home/keysanadea/SISOP/NO4/soal4decrypt/".

#### OUTPUT:
**Encrypt**
![Screenshot_2023-03-10_205334](/uploads/2c81748d24485f3da34a169e9ca0e15f/Screenshot_2023-03-10_205334.png)

**Decrypt**
![Screenshot_2023-03-10_205522](/uploads/eb8cb7079a337807a86ad8e0f4993674/Screenshot_2023-03-10_205522.png)
