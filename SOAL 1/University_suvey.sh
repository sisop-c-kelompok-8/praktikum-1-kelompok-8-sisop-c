echo 1.A 
grep "Japan" 2023UNIRANK.csv  | sort -t ',' -k1 -n | head -5 |awk -F ',' '{print $1, $2}' 
echo

echo 1.B
grep "Japan" 2023UNIRANK.csv  | tail -5 | sort -t ',' -k9 -n |awk -F ',' '{print $1, $2, $9}' 
echo

echo 1.C
grep "Japan" 2023UNIRANK.csv  | sort -t ',' -k20 -n | head -10 |awk -F ',' '{print $1, $2, $4, $20}' 
echo

echo 1.D
awk -f ',' '/Keren/ '{print $1, $2}' 2023UNIRANK.csv
echo