#!/bin/bash

jml_zip=$(ls -d devil_* 2>/dev/null | wc -l)
zip_name="devil_$((jml_zip + 1))"

folder_count=$(ls -d kumpulan_* 2>/dev/null | wc -l)
let folder_count2=$folder_count-1
let folder_count3=$folder_count2-1

if [ $(expr $folder_count % 5) == 3 ]
then
    zip -r $zip_name kumpulan_$folder_count kumpulan_$folder_count2 kumpulan_$folder_count3
elif [ $(expr $folder_count % 5) == 0 ]
then
    zip -r $zip_name kumpulan_$folder_count kumpulan_$folder_count2
fi

hour=$(date +"%H")
if [ "$hour" == 00 ]; then
    countx=1
else
    countx=$hour
fi

if [ "$folder_count" -eq "0" ]; then
    nama_folder="kumpulan_1"
else
    next_number=$((folder_count + 1))
    nama_folder="kumpulan_${next_number}"
fi

mkdir -p "$nama_folder"

echo "file akan didownload sebanyak $countx kali"

index=1
while [ "$index" -le "$countx" ]; do
    name_file="perjalanan_$index.jpg"
    index=$((index+1))
    wget -O "$nama_folder/$name_file" https://artikula.id/wp-content/uploads/2021/12/wayang.jpg
done

# 0 */10 * * * /home/anneutsabita/kobeni_liburan.sh
